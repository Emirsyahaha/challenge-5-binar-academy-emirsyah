const express = require("express");
const path = require("path");
const app = express();
const port = process.env.PORT || 3000;

const usersData = require(".user.json")

app.use(express.static("./Challenge3"));

app.get("/", (request, response) => {
  response.sendFile(path.join(__dirname, "/Challenge3/index.html"));
});

app.listen(port);
console.log("Server started at http://localhost:" + port);
