const express = require("express");
const path = require("path");
const app = express();
const port = process.env.PORT || 3001;

app.use(express.static("./Challenge4"));

app.get("/", (request, response) => {
  response.sendFile(path.join(__dirname, "/Challenge4/suit.html"));
});

app.listen(port);
console.log("Server started at http://localhost:" + port);
